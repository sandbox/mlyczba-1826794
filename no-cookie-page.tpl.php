<?php

/**
 * @file
 * Template for the popup page for the Cookie Law Server side popup module.
 */

print t('This website makes usage of tracking cookies!') ?><br/><br/>

<a href="/accept-cookies/"><?php print t('I accept the cookies that are set on this website.') ?></a>
